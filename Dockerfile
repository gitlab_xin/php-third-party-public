FROM registry.cn-shenzhen.aliyuncs.com/zhaoxin_space/nginx-php:7.1

ENV VIRTUAL_HOST third-party.birdnight.cn
ENV LETSENCRYPT_HOST third-party.birdnight.cn

COPY . /app


#RUN chmod -R 777 /app/runtime
#RUN chmod -R 777 /app/public/qrcode
RUN chmod -R 777 /app
