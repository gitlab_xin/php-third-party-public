<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/4/14
 * Time: 10:13
 */
// +----------------------------------------------------------------------
// | 微信设置
// +----------------------------------------------------------------------

return [
    //订单详情
    'url' => 'https://m.xaishop.com/mine/order/detail?uuid=',

    'Pay' => [
        'MchId' => "1524549281",
        'Key' => '565A7C0810BF6C32F44385B998466A33'
    ],

    //公众号
    'PublicAppID' => 'wx787d4885b7bf6fb2',  //
    'PublicAppSecret' => "cc2e7c1525fd9629bca570ef23a627af",//


    // 'PublicAppID' => 'wx787d4885b7bf6fb2',
    // 'PublicAppSecret' => "cc2e7c1525fd9629bca570ef23a627af",
    'PublicPay' => [
        'MchId' => "1524549281",
        'Key' => '565A7C0810BF6C32F44385B998466A33'
    ],

    // https://mp.weixin.qq.com/wiki?action=doc&id=mp1433751277#5
    'template' => [
        'ship' =>
            [//订单发货通知
                'template_id' => 'bQblNjGtbVNa3sKbfOmUj1169rp6XFCj7OmqUgiMucM',
                'data'=>['first','keyword1','keyword2','keyword3','remark']
            ],
/*
{{first.DATA}}
订单编号：{{keyword1.DATA}}
快递名称：{{keyword2.DATA}}
快递单号：{{keyword3.DATA}}
{{remark.DATA}}
exp:
**同学，您的教材订单已经发货。
订单编号：L2017110210000994241
快递名称：优速物流
快递单号：584311244646
请小主耐心等待，也可以登录官网查询物流最新消息，感谢您的使用。
 */
        'pay' =>
            [//支付成功通知
                'template_id' => 'DucNyboSj6jWdOZ8Y0w92Hhi-nmU-9lEqohaORRJAJg',
                'data'=>['first','keyword1','keyword2','keyword3','remark']
            ],
        /*
{{first.DATA}}
支付金额：{{keyword1.DATA}}
支付方式：{{keyword2.DATA}}
支付说明：{{keyword3.DATA}}
{{remark.DATA}}
exp:
您好，您于1月9日11时11分消费的订单支付成功
支付金额：￥0.01
支付方式：微信支付
支付说明：餐厅点餐
感谢您的使用，点击查看详情！
         */
        'refund' =>
            [//退款结果提醒
                'template_id' => 'zVjAc43bIQauAytW-vXFVo6SpbkVqbdNlUkZZvkonPw',
                'data'=>['first','keyword1','keyword2','keyword3','keyword4','remark']
            ],
        // 'sign' =>
        //     [//上课签到完成提醒
        //         'template_id' => 'GtTYQqM3iHda4mtVr4C6n5y4DC5hN_Lrs_G-xj2Fkug',
        //         'data'=>['first','keyword1','keyword2','keyword3','remark']
        //     ],
        'sign' =>
            [//上课签到完成提醒
                'template_id' => 'lHnsrKOCfitiWXsKmvtX7od8YAcNBmBHE0d9pjkfeuM',
                'data'=>['first','keyword1','keyword2','keyword3','remark']
            ],
/*
{{first.DATA}}
姓名：{{keyword1.DATA}}
班级：{{keyword2.DATA}}
签到时间：{{keyword3.DATA}}
{{remark.DATA}}
exp:
家长您好，您的孩子今日已完成签到。
姓名：张三
班级：17级01班
签到时间：2017/08/31 16:34:15
点击查看签到详情。
 */
        'activity' =>
            [//活动报名成功通知
                'template_id' => '0XAiLAkq8I4CHXWjbYrwu829gM1y4V0MmyyuHNlBI2I',
                'data'=>['first','keyword1','keyword2','keyword3','remark']
            ],
/*
{{first.DATA}}
活动名称：{{keyword1.DATA}}
活动时间安排：{{keyword2.DATA}}
活动地点：{{keyword3.DATA}}
{{remark.DATA}}
exp:
你好，活动报名成功
活动名称：吴敬莲教授经济论坛
活动时间安排：2014年10月18日
活动地点：中欧会议厅
请您准时参加！
*/

        'seller' =>
            [//通知商家订单消息
                'template_id' => 'PD7KXComNtq4cfiwRMhHJDmUNa02q45J4gNQHqpO7zA',
                'data'=>['first','keyword1','keyword2','keyword3','remark']
            ]
/*
{{first.DATA}}
活动名称：{{keyword1.DATA}}
活动时间安排：{{keyword2.DATA}}
活动地点：{{keyword3.DATA}}
{{remark.DATA}}
exp:
某某商家：您有新订单啦
订单类目：商品名称...
订单时间：2014年10月18日
订单内容：您需要及时处理
请及时查看
*/

    ]
];