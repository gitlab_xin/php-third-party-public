<?php
/**
 * Created by PhpStorm.
 * User: fio
 * Date: 2017/2/21
 * Time: 下午4:47
 */

namespace app\common\tools;

class wechatMessage
{
    private $appId = '';
    private $secret = '';

    public function __construct($appId, $secret)
    {
        $this->setAppId($appId);
        $this->setSecret($secret);
    }

    /**
     * @author: Airon
     * @time:   2019年4月
     * description $data 传参请参考wechat配置文件消息模版
     * @param $openid
     * @param $data ['first','remark','keyword1','keyword2'···]
     * @param string $type ship 发货通知|pay 支付成功通知|refund 退款结果通知|sign 上课签到通知|activity 活动报名成功
     * @return bool
     */
    public function send($openid,$data,$type,$view_url = ''){
        $config = config('wechat')['template'];
        $template = $config[$type];
        $template_id = $template['template_id'];
        $template_data = $template['data'];

        $access_token = wechatUtil::getPublicAccessToken($this->appId,$this->secret);
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$access_token;

        $postData['touser'] = $openid;
        $postData['template_id'] = $template_id;
        if(isset($view_url) && !empty($view_url)){
            $postData['url'] = config('wechat')['url'].$view_url;
        }

        foreach ($template_data as $value){
            $postData['data'][$value] = ['value'=>$data[$value]];
        }
        $result = curl_post($url,$postData);//dump($result);
        if (!empty($result['errcode'])) {
            return false;
        }
        return true;
    }

    /**
     * @param string $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    }

    /**
     * @param string $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

}
