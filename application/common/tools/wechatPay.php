<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\common\tools;

use think\Request;
use app\common\tools\wechatUtil;

class wechatPay
{
    private $appId = '';
    private $mchId = '';
    private $appPayKey = '';
    public $notifyUrl = PUBLIC_PATH . '/v1/app/Callback/wxPay';
    public $unifiedOrderUrl = '';
    public $refundUrl = '';
    public $transfresUrl = '';
    public $errorMessage = '';
    public $errorDetail;
    public $logPath = '';

    public function __construct($appId, $mchId, $payKey)
    {
        $this->setAppId($appId);
        $this->setMchId($mchId);
        $this->setAppPayKey($payKey);
        $this->unifiedOrderUrl = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
        $this->refundUrl = 'https://api.mch.weixin.qq.com/secapi/pay/refund';
        $this->transfresUrl = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers';
        $this->logPath = LOG_PATH;
    }

    public function order($outTradeNo, $fee, $body, $attach = '', $tradeType,$notifyUrl = "",$openid = "")
    {
//        $fee = 0.01;//todo 测试
        if(!empty($notifyUrl)){
            $this->notifyUrl = $notifyUrl;
        }
        $xmlData = $this->getWeChatOrderData($outTradeNo, $fee * 100, $body, $attach, $tradeType,$openid);
        $result = wechatUtil::postCurl($this->unifiedOrderUrl, $xmlData);
        if (empty($result)) {
            $this->setErrorInfo('下单失败', '请求订单失败');
            return false;
        }
        $result = wechatUtil::xmlParser($result);
        if (empty($result)) {
            $this->setErrorInfo('下单失败', '订单解析失败');
            return false;
        }
        if ($result['return_code'] != 'SUCCESS' || $result['return_msg'] != 'OK') {
            $this->setErrorInfo('下单失败', $result['return_msg']);
            return false;
        }
        // build request json, 返回给前端调用支付页面
        if($tradeType != 'JSAPI'){
//            $requestData = [
//                'mweb_url' => $result['mweb_url']
//            ];
            if($tradeType == 'MWEB'){
                $requestData = [
                    'mweb_url' => $result['mweb_url']
                ];
            }else{
                $requestData = [
                    'appid' => $this->appId,
                    'package' => 'Sign=WXPay',
                    'noncestr' => wechatUtil::getRandomString(32),
                    'timestamp' => time(),
                    'sign' => "",
                    'prepayid' => $result['prepay_id'],
                    'partnerid' => $this->mchId
                ];
                if($tradeType == 'NATIVE'){
                    $requestData['out_trade_no'] = $outTradeNo;
                    $requestData['code_url'] = $result['code_url'];
                }
                $requestData['sign'] = wechatUtil::makeSign($requestData, $this->appPayKey);
            }
        }else{
            $requestData = [
                'appId' => $this->appId,
                'timeStamp' => time(),
                'nonceStr' => wechatUtil::getRandomString(32),
                'package' => 'prepay_id=' . $result['prepay_id'],
                'signType' => 'MD5',
                'paySign' => '',
            ];
            $requestData['paySign'] = wechatUtil::makeSign($requestData, $this->appPayKey);
        }

        return $requestData;
    }

    /**
     * @author: Airon
     * @time: 2017年8月11日
     * description:退款
     * @param string $outRefundNo 退款单号
     * @param string $outTradeNo 支付单号
     * @param float $total_fee 支付订单总金额
     * @param float $fee 本次退款金额
     * @return bool
     */
    public function refund($outRefundNo, $outTradeNo, $total_fee, $fee)
    {

//        $total_fee = $fee = 0.01;//todo 测试
        $xmlData = $this->getWeChatRefundData($outRefundNo, $outTradeNo, $total_fee * 100, $fee * 100);
        $result = wechatUtil::curl_post_ssl($this->refundUrl, $xmlData);

        if (empty($result)) {
            $this->setErrorInfo('退款失败', '请求退款失败');
            return false;
        }
        $result = wechatUtil::xmlParser($result);
        if (empty($result)) {
            $this->setErrorInfo('退款失败', '退款单解析失败');
            return false;
        }
        if ($result['return_code'] != 'SUCCESS') {
            $this->setErrorInfo('退款失败', $result['return_msg']);
            return false;
        }
        if ($result['return_code'] != 'SUCCESS' || $result['result_code'] != 'SUCCESS') {
            $this->setErrorInfo('退款失败', $result['return_msg']);
            return false;
        }
        return true;
    }

    /**
     * @author: Airon
     * @time: 2017年8月11日
     * description:打款到零钱
     * @param string $outTransferNo 打款单号
     * @param int $amount 打款金额
     * @param string $desc 企业付款备注
     * @param string $openid 用户openid
     * @return bool
     */
    public function transfers($outTransferNo, $amount, $openid, $desc)
    {
        $xmlData = $this->getWeChatTransferData($outTransferNo, $amount * 100, $openid, $desc);
        $result = wechatUtil::curl_post_ssl($this->transfresUrl, $xmlData);
//        echo $result;
        if (empty($result)) {
            $this->setErrorInfo('打款失败', '请求打款失败');
            return false;
        }
        $result = wechatUtil::xmlParser($result);
        if (empty($result)) {
            $this->setErrorInfo('打款失败', '打款单解析失败');
            return false;
        }
        if ($result['return_code'] != 'SUCCESS') {
            $this->setErrorInfo('打款失败', $result['return_msg']);
            return false;
        }
        if ($result['return_code'] != 'SUCCESS' || $result['result_code'] != 'SUCCESS') {
            $this->setErrorInfo('打款失败', $result['return_msg']);
            return false;
        }
        return true;
    }

    public function handleNotify($requestStr)
    {
        $requestArray = wechatUtil::xmlParser($requestStr);

        if (empty($requestArray) || !isset($requestArray['return_code'])) {
            wechatUtil::log_file($requestArray, 'parse error', 'wxpay', $this->logPath);
            return false;
        }
        if ($requestArray['return_code'] != 'SUCCESS') {
            wechatUtil::log_file($requestArray, 'return code error', 'wxpay', $this->logPath);
            return false;
        }

        $realSign = wechatUtil::makeSign($requestArray, $this->appPayKey);

        if ($realSign != $requestArray['sign']) {
            // sign fail
            wechatUtil::log_file($requestArray, 'sign fail:' . $realSign, 'wxpay', $this->logPath);
            return false;
        }

        if ($requestArray['result_code'] != 'SUCCESS') {
            // pay fail
            wechatUtil::log_file($requestArray, 'result is fail', 'wxpay', $this->logPath);
            return false;
        }

        return $requestArray;
    }

    private function getWeChatOrderData($outTradeNo, $fee, $body, $attach, $tradeType,$userOpenId = "")
    {
        $orderData = [
            'appid' => $this->appId, // 公众账号ID
            'mch_id' => $this->mchId, // 商户号
            'device_info' => 'WEB',
            'nonce_str' => wechatUtil::getRandomString(32), // 随机字符串
            'sign' => '',
            'sign_type' => 'MD5',
            'body' => $body,
            'attach' => '' . $attach, // 自定义参数
            'out_trade_no' => $outTradeNo, // 商户系统内部订单号
            'fee_type' => 'CNY',
            'total_fee' => $fee,
            'spbill_create_ip' => get_client_ip(),
            'notify_url' => $this->notifyUrl,
            'trade_type' => $tradeType,
//            'prepay_id' => $prepay_id,
        ];
        if(!empty($userOpenId) && $tradeType == "JSAPI"){
            $orderData['openid'] = $userOpenId;
        }
        if ($orderData['spbill_create_ip'] == '::1') {
            $orderData['spbill_create_ip'] = '127.0.0.1';
        }
        $orderData['sign'] = wechatUtil::makeSign($orderData, $this->appPayKey);
        $result = wechatUtil::toXml($orderData);
        return $result;
    }

    private function getWeChatRefundData($out_refund_no, $outTradeNo, $total_fee, $fee)
    {
        $orderData = [
            'appid' => $this->appId, // 公众账号ID
            'mch_id' => $this->mchId, // 商户号
            'op_user_id' => $this->mchId,// 商户号
            'nonce_str' => wechatUtil::getRandomString(32), // 随机字符串
            'sign' => '',
            'sign_type' => 'MD5',
            'out_trade_no' => $outTradeNo, // 商户系统内部订单号
            'out_refund_no' => $out_refund_no, // 商户系统内部退款单号
            'fee_type' => 'CNY',
            'total_fee' => $total_fee,//订单金额
            'refund_fee' => $fee,//退款金额
            'refund_account' => "REFUND_SOURCE_RECHARGE_FUNDS",//退款资金来源 默认为未结算资金 当前设置为余额资金
        ];
        $orderData['sign'] = wechatUtil::makeSign($orderData, $this->appPayKey);
        $result = wechatUtil::toXml($orderData);
        return $result;
    }

    private function getWeChatTransferData($outTransferNo, $amount, $openid,$desc)
    {
        $orderData = [
            'mch_appid' => $this->appId, // 公众账号ID
            'mchid' => $this->mchId, // 商户号
            'nonce_str' => wechatUtil::getRandomString(32), // 随机字符串
            'sign' => '',
            'partner_trade_no' => $outTransferNo, //商户订单号
            'openid' => $openid, // 用户openid
            'check_name' => 'NO_CHECK',//校验用户姓名选项
            'amount' => $amount,//金额
            'desc' => $desc,//企业付款备注
            'spbill_create_ip' => get_client_ip(),
        ];
        if ($orderData['spbill_create_ip'] == '::1') {
            $orderData['spbill_create_ip'] = '127.0.0.1';
        }
        $orderData['sign'] = wechatUtil::makeSign($orderData, $this->appPayKey);
        $result = wechatUtil::toXml($orderData);
//        echo $result;
        return $result;
    }

    public function setErrorInfo($message, $detail)
    {
        $this->errorMessage = $message;
        $this->errorDetail = $detail;
    }

    /**
     * @param string $notifyUrl
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * @param string $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    }

    /**
     * @param string $mchId
     */
    public function setMchId($mchId)
    {
        $this->mchId = $mchId;
    }

    /**
     * @param string $appPayKey
     */
    public function setAppPayKey($appPayKey)
    {
        $this->appPayKey = $appPayKey;
    }

}
