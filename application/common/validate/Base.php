<?php
/**
 * Created by PhpStorm.
 * User: airon
 */

namespace app\common\validate;

use think\Validate;

class Base extends Validate
{
    protected $regex = [
        'mobile' => '1\d{10}'
    ];

    protected $rule = [
        'page_index' => 'require|number|gt:0',
        'page_size' => 'require|number|gt:0',
        'uuid' => 'require|length:32'
    ];

    protected $field = [
        'mobile' => '手机号码'
    ];

    protected $message = [
        'page_index' => '页码要大于0',
        'page_size' => '每页数量要大于0',
        'id' => 'id参数不符合'
    ];

    protected $scene = [
        'page_index' => ['page_index'],
        'page_size' => ['page_size'],
        'page' => ['page_index', 'page_size']
    ];

}
