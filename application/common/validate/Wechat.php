<?php
namespace app\common\validate;

use think\Validate;

class Wechat extends Validate
{
    protected $regex = [
        'appid' => 'wx[a-z0-9]{16}'
    ];
    protected $rule = [
        'appid' => 'require|length:18|alphaNum|regex:appid',
        'secret' => 'require|length:32'
    ];

    protected $scene = [
        'index' => ['appid','secret']
    ];
}