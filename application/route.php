<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Request;
use think\Route;

Route::resource(':version/app/Wechat', 'api/:version.app.Wechat');
Route::post(':version/app/Wechat/public-access-token', 'api/:version.app.Wechat/publicAccessToken');
Route::post(':version/app/Wechat/jsapi-ticket', 'api/:version.app.Wechat/jsapiTicket');

Route::miss('Error/index');
$request = Request::instance();
if ($request->method() === "OPTIONS") {
    exit (json_encode(array('error' => 200, 'message' => 'Options Success.')));
}
return [
    '__pattern__' => [
        'name' => '\w+',
    ],


];
