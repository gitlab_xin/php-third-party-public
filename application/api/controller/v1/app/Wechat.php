<?php
/**
 * Created by PhpStorm.
 * User: xiezhixian
 * Date: 2019-08-12
 * Time: 10:04
 */
namespace app\api\controller\v1\app;

use app\common\tools\RedisUtil;
use app\common\tools\wechatUtil;
use think\Collection;
use think\Db;
use think\Exception;

class Wechat extends Base{

    public $restMethodList = 'get|post';

    public function publicAccessToken(){
        $request = $this->selectParam(['appid','secret']);
        $this->check($request,"wechat.index");
        $wechatUtil = new wechatUtil();
        $publicAccessToken = $wechatUtil->getPublicAccessToken($request['appid'],$request['secret']);
        $this->render(['public_access_token'=>$publicAccessToken]);

    }

    public function jsapiTicket(){
        $request = $this->selectParam(['appid','secret']);
        $this->check($request,"wechat.index");
        $wechatUtil = new wechatUtil();
        $publicAccessToken = $wechatUtil->getJSAPITicket($request['appid'],$request['secret']);
        $this->render(['jsapi_ticket'=>$publicAccessToken]);
    }

}